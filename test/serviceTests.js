const Service = require('../src/service');
const ErrorResponse = require('../src/errorResponse');
const assert = require('chai').assert;
const helper = require('./helper');
const Event = require('../src/event');
const Route = require('../src/route');
const Policy = require('../src/policy/policy');
const http = require('http');
const Config = require('../src/config');
const LogLevel = require('../src/logLevel')

class TestService extends Service {
    constructor(config = Config.load("./serviceTestsConfig.yaml"))
    {
        super(config);
        this.on('error', function(e){}); //Suppress emit("error") events
    }
    async exposedHandleEvent(event)
    {
        await this._handleEvent(event);
    }
    async exposedHandleError(event, error)
    {
        await this._handleError(event, error);
    }
};

describe('App', function(){
    describe('Logging', ()=>{
        let traceEmitted = 0;
        let service

        beforeEach(()=>{
            traceEmitted = 0;

            service = new TestService();
            service.on(LogLevel.Trace, ()=>traceEmitted++)
        })
        
        it('Logging is on by default', async function(){
            
            let event = new Event(helper.createRequest('get', '/logging_on'));

            await service.process(event)

            assert.equal(traceEmitted, 2)
        });

        it('Logging is off', async function(){
            
            let event = new Event(helper.createRequest('get', '/logging_off'));

            await service.process(event)

            assert.equal(traceEmitted, 0)
        });

        it('Logging is trace', async function(){
            
            let event = new Event(helper.createRequest('get', '/logging_trace'));

            await service.process(event)

            assert.equal(traceEmitted, 2)
        });

        it('Logging is info', async function(){
            
            let event = new Event(helper.createRequest('get', '/logging_info'));

            await service.process(event)

            assert.equal(traceEmitted, 0)
        });

        it('Logging is debug', async function(){
            
            let event = new Event(helper.createRequest('get', '/logging_debug'));

            await service.process(event)

            assert.equal(traceEmitted, 2)
        });

        it('Logging is error', async function(){
            
            let event = new Event(helper.createRequest('get', '/logging_error'));

            await service.process(event)

            assert.equal(traceEmitted, 0)
        });

        it('Logging is trace; handler is none', async function(){
            
            let event = new Event(helper.createRequest('get', '/logging_trace_handler_none'));

            await service.process(event)

            assert.equal(traceEmitted, 1)
        });

        it('Logging is none; handler is trace', async function(){
            
            let event = new Event(helper.createRequest('get', '/logging_none_handler_trace'));

            await service.process(event)

            assert.equal(traceEmitted, 0)
        });
    });
    describe('handleEvent()', function () {
        it('Handle an event successfully', async function(){
            
            let event = new Event(helper.createRequest('get', '/collections'));
            
            await new TestService().process(event)

            assert.equal('Hi there', event.response.body);
            assert.equal(201, event.response.statusCode);
        });
        it('Handle an event with "complete" state (promise) successfully', async function(){
            
            let event = new Event(helper.createRequest('post', '/collections'));
            
            await new TestService().process(event)
            
            assert.equal('This is an expected response.', event.response.body);
        });
        it('Handle an event with "complete" state successfully', async function(){
            
            let event = new Event(helper.createRequest('delete', '/collections'));
            
            await new TestService().process(event)
            
            assert.equal('This is an expected response.', event.response.body);
        });
        it('Handle an emittable handler', async function(){
            
            let event = new Event(helper.createRequest('get', '/emittable'));
            
            await new TestService().process(event)
        });
        it('Handle an exception', async function()
        {
            let event = new Event(helper.createRequest('get', '/collections/1'));

            try
            {
                await new TestService().process(event)
            }
            catch(e)
            {
                assert.fail(e)
            }

            assert.instanceOf(event.response, ErrorResponse);
            assert.equal(500, event.response.statusCode);
            assert.equal('Error!', event.response.message);
        });
    });
    describe('handleError()', function(){
        it('Handle ErrorResponse', async function(){
            
            let ev = new Event(helper.createRequest('get', '/collections'));

            let app = new TestService();
            
            await app.exposedHandleError(ev, new ErrorResponse("This is an ErrorResponse", 400, "Details"));

            assert.instanceOf(ev.response, ErrorResponse);
            assert.equal(400, ev.response.statusCode);
            assert.equal("This is an ErrorResponse", ev.response.body.message);
            assert.equal("Details", ev.response.body.details);
        });
        it('Handle Error', async function(){
            let ev = new Event(helper.createRequest('get', '/collections'));

            let app = new TestService();

            await app.exposedHandleError(ev, new Error("This is an Error"));

            assert.instanceOf(ev.response, ErrorResponse);
            assert.equal(500, ev.response.statusCode);
            assert.equal("This is an Error", ev.response.body.message);
            assert.isTrue(ev.response.body.details.length > 0);
        });
        it('Handle any errors', async function(){
            let ev = new Event(helper.createRequest('get', '/collections'));

            let app = new TestService();

            await app.exposedHandleError(ev, -1);

            assert.instanceOf(ev.response, ErrorResponse);
            assert.equal(500, ev.response.statusCode);
            assert.equal('Internal Server Error', ev.response.body.message);
            assert.equal(-1, ev.response.body.details);
        });
        it('Handle errors with custom error handler', async function(){
            let ev = new Event(helper.createRequest('get', '/collections'));

            let app = new TestService(Config.load("./serviceTestsConfigWithCustomErrorHandler.yaml"));
            
            await app.exposedHandleError(ev, "MyCustomError");
                        
            assert.equal(400, ev.response.statusCode);
            assert.equal('This is a custom error body', ev.response.body);
        });
        it('Handle errors with on_events', async function(){
            let ev = new Event(helper.createRequest('get', '/collections'));

            let app = new TestService(Config.load("./serviceTestsConfigWithOnEvents.yaml"));
            
            await app.process(ev)
            
            assert.equal(ev.response.body, 'ABCE');
        });
        it('Handle errors with on_events - event not found', async function(){
            let ev = new Event(helper.createRequest('options', '/collections'));

            let app = new TestService(Config.load("./serviceTestsConfigWithOnEvents.yaml"));
            
            await app.process(ev)
            
            assert.equal(ev.response.body, '[object Object]DE'); //The [object Object] is the ErrorResponse.
        });
        it('Handle errors with method events', async function(){
            let ev = new Event(helper.createRequest('options', '/collections'));

            let app = new TestService(Config.load("./serviceTestsConfigWithMethodEvents.yaml"));
            
            await app.process(ev)
            
            assert.equal(ev.response.statusCode, 204); 
        });
    });
});