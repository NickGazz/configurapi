const Config = require('../src/config');
const assert = require('chai').assert;
const LogLevel = require('../src/logLevel');

const configFile = "\
--- \n\
import: \n\
    - module1 \n\
    - module2 \n\
global: \n\
    - &var1 value1 \n\
    - &var2 value2 \n\
api: \n\
    events: \n\
        - name: name_route_1 \n\
          logLevel: none\n\
          policies: \n\
              - policy_name1: \n\
                    - p1 \n\
                    - *var1 \n\
                    - *var2 \n\
              - policy_name2 \n\
";
const configNewPolicyFormatFile = "\
--- \n\
api: \n\
    events: \n\
        - name: name_route_1 \n\
          policies: \n\
              - policy_name1: \n\
                logLevel: none \n\
                parameters: \n\
                    - p1 \n\
                    - p2 \n\
              - policy_name2 \n\
";

const configFileWithConditionalRoutes = "\
--- \n\
import: \n\
    - module1 \n\
    - module2 \n\
global: \n\
    - &var1 value1 \n\
    - &var2 value2 \n\
api: \n\
    events: \n\
        - name: name_route_1 \n\
          when: my_env \n\
          policies: \n\
              - policy_name1 \n\
        - name: name_route_2 \n\
          policies: \n\
              - policy_name2 \n\
";

const notArrayInRoutes = "\
--- \n\
global: \n\
    - &var1 value1\n\
    - &var2 value2\n\
api: \n\
    events: \n\
         policies: \n\
              - policy_name1 \n\
              - policy_name2 \n\
";

const missingName = "\
--- \n\
global: \n\
    - &var1 value1\n\
    - &var2 value2\n\
api: \n\
    events: \n\
       - policies: \n\
              - policy_name1 \n\
              - policy_name2 \n\
";

const notArrayInPolicies = "\
--- \n\
global: \n\
    - &var1 value1\n\
    - &var2 value2\n\
api: \n\
    events: \n\
       - name: policy1 \n\
         policies: 3 \n\
";

const missingPolicies = "\
--- \n\
global: \n\
    - &var1 value1\n\
    - &var2 value2\n\
api: \n\
    events: \n\
       - name: policy1 \n\
";
const missingPolicy = "\
--- \n\
global: \n\
    - &var1 value1\n\
    - &var2 value2\n\
api: \n\
    events: \n\
       - name: policy1 \n\
         policies: \n\
            - 0\n\
";
const notArrayInImport = "\
--- \n\
import: false \n\
api: \n\
    events: \n\
       - name: dummy  \n\
         policies: \n\
            - dummy \n\
";

describe('Config', function(){
    describe('parse', function(){
        
        beforeEach(()=>{
            process.env['NODE_ENV'] = undefined;
        });
        
        it('File not found', function(){
            assert.throw(()=>Config.load("notexisting"), "Failed to load 'notexisting'");
        });
        it('Invalid yaml', function(){
            assert.throw(()=>Config.parse("<xml/>"), "The specified config file is not an invalid yaml.");
        });
        it('Api not found', function(){
            assert.throw(()=>Config.parse("a: b"), "Could not find 'api' in the config document");
        });
        it('Events not found', function(){
            assert.throw(()=>Config.parse("api: something"), "Could not find 'api.events' in the config document");
        });
        it('Events not array', function(){
            assert.throw(()=>Config.parse(notArrayInRoutes), 'Events could not be empty in the config document.');
        });
        it('Event name not found', function(){
            assert.throw(()=>Config.parse(missingName), "Could not find 'api.events[0].name' in the config document");
        });
        it('Policies not array', function(){
            assert.throw(()=>Config.parse(notArrayInPolicies), "Policies 'api.events[policy1].policies' could not be empty in the config document.");
        });
        it('Policies not found', function(){
            assert.throw(()=>Config.parse(missingPolicies), "Could not find 'api.events[policy1].policies' in the config document");
        });
        it('Policy not found', function(){
            assert.throw(()=>Config.parse(missingPolicy), "Could not find 'api.events[policy1].policies[0]' in the config document");
        });
        it('Import not array', function()
        {
            assert.throw(()=>Config.parse(notArrayInImport), "Could not load 'import.false' in the config document");
        });
        it('Valid config', function(){
            let config = Config.parse(configFile);
            
            assert.equal(2, config.modules.length, "assert a number of imported modules");
            assert.equal('module1', config.modules[0], "assert an imported module");
            assert.equal(1, config.events.length, "assert a number of events");
            assert.isTrue(config.events.has('name_route_1'), "assert a event");
            assert.equal(config.events.get('name_route_1').logLevel, LogLevel.None);
            assert.equal(2, config.events.get('name_route_1').policies.length, "assert a number of policies");
            assert.equal('policy_name1', config.events.get('name_route_1').policies[0].name, "assert a policy");
            assert.equal(3, config.events.get('name_route_1').policies[0].parameters.length, "assert a policy's parameters");
            assert.equal('value1', config.events.get('name_route_1').policies[0].parameters[1], "assert a parameter");
            //console.log(config.events.get('name_route_1').policies[0]);
        });
        it('Valid config - New policy format', function(){
            let config = Config.parse(configNewPolicyFormatFile);
            
            assert.equal(1, config.events.length, "assert a number of events");
            assert.isTrue(config.events.has('name_route_1'), "assert a event");
            assert.equal(2, config.events.get('name_route_1').policies.length, "assert a number of policies");
            assert.equal('policy_name1', config.events.get('name_route_1').policies[0].name, "assert a policy");
            assert.equal(LogLevel.None, config.events.get('name_route_1').policies[0].logLevel);
            assert.equal(2, config.events.get('name_route_1').policies[0].parameters.length, "assert a policy's parameters");
            assert.equal('p1', config.events.get('name_route_1').policies[0].parameters[0], "assert a parameter");
            assert.equal('p2', config.events.get('name_route_1').policies[0].parameters[1], "assert a parameter");
            assert.equal('policy_name2', config.events.get('name_route_1').policies[1].name, "assert a policy");
        });
        it('Valid config with conditional routes - false', function(){
            let config = Config.parse(configFileWithConditionalRoutes);
            
            assert.equal(2, config.modules.length, "assert a number of imported modules");
            assert.equal('module1', config.modules[0], "assert an imported module");
            assert.equal(1, config.events.length, "assert a number of events");
            assert.isFalse(config.events.has('name_route_1'), "assert a event");
            assert.isTrue(config.events.has('name_route_2'), "assert a event");
            assert.equal(1, config.events.get('name_route_2').policies.length, "assert a number of policies");
            assert.equal('policy_name2', config.events.get('name_route_2').policies[0].name, "assert a policy");
        });
        it('Valid config with conditional routes - true', function(){
            process.env['NODE_ENV'] = 'my_env';
            let config = Config.parse(configFileWithConditionalRoutes);
            
            assert.equal(2, config.modules.length, "assert a number of imported modules");
            assert.equal('module1', config.modules[0], "assert an imported module");
            assert.equal(2, config.events.length, "assert a number of events");
            assert.isTrue(config.events.has('name_route_1'), "assert a event");
            assert.equal(1, config.events.get('name_route_1').policies.length, "assert a number of policies");
            assert.equal('policy_name1', config.events.get('name_route_1').policies[0].name, "assert a policy");
            assert.isTrue(config.events.has('name_route_2'), "assert a event");
            assert.equal(1, config.events.get('name_route_2').policies.length, "assert a number of policies");
            assert.equal('policy_name2', config.events.get('name_route_2').policies[0].name, "assert a policy");
        });
    });
});