const assert = require('chai').assert;
let Identity = require('../src/identity');

describe('Identity', function() 
{
    it("hasClaims", function()
    {
        let identity = new Identity();
        identity.claims = [{type:'t1', value:'v1'}];

        assert.isTrue(identity.hasClaim('t1', 'v1'));
        assert.isFalse(identity.hasClaim('t1', 'x'));
        assert.isFalse(identity.hasClaim('x', 'v1'));
        assert.isFalse(identity.hasClaim('x', 'x'));
    });

    it("hasClaims with scope", function()
    {
        let identity = new Identity();
        identity.claims = [{type:'t1', value:'v1', scope: ['s11','s12','s13']}, {type:'t2', value:'v2', scope: 's2'}, {type:'t3', value:'v3'}];

        assert.isFalse(identity.hasClaim('t1', 'v1'));
        assert.isFalse(identity.hasClaim('t2', 'v2'));
        assert.isTrue(identity.hasClaim('t3', 'v3'));
        assert.isTrue(identity.hasClaim('t3', 'v3', '')); //Empty scope === ''
        assert.isTrue(identity.hasClaim('t1', 'v1', 's11'));
        assert.isTrue(identity.hasClaim('t1', 'v1', 's12'));
        assert.isTrue(identity.hasClaim('t1', 'v1', 's13'));
        assert.isFalse(identity.hasClaim('t1', 'v1', 's2'));
        assert.isTrue(identity.hasClaim('t2', 'v2', 's2'));
        assert.isFalse(identity.hasClaim('t2', 'v2', 's11'));
        assert.isFalse(identity.hasClaim('t2', 'v2', 's12'));
        assert.isFalse(identity.hasClaim('t2', 'v2', 's13'));
        assert.isFalse(identity.hasClaim('t3', 'v3', 's3'));
    });

    it("hasClaims multiple scopes", function()
    {
        let identity = new Identity();
        identity.claims = [{type:'t1', value:'v1', scope: 's1'}, {type:'t2', value:'v2', scope: 's2'}, {type:'t3', value:'v3'}];

        assert.isFalse(identity.hasClaim('t1', 'v1', ['s2', 's3']));
        assert.isFalse(identity.hasClaim('t1', 'v1'));
        assert.isTrue(identity.hasClaim('t1', 'v1', ['sX', 's1']));
        assert.isTrue(identity.hasClaim('t3', 'v3', ['s1', '']));
    });

    it("hasClaims multiple values", function()
    {
        let identity = new Identity();
        identity.claims = [{type:'t1', value:'v1', scope: 's1'}, {type:'t1', value:'v2', scope: 's2'}, {type:'t3', value:'v3'}];

        assert.isFalse(identity.hasClaim('t1', ['v1', 'v2']));
        assert.isTrue(identity.hasClaim('t1', ['v1', 'v2'], 's2'));
        assert.isTrue(identity.hasClaim('t1', ['v1', 'v2'], 's1'));
        assert.isFalse(identity.hasClaim('t1', ['v1', 'v2'], 's3'));
        assert.isTrue(identity.hasClaim('t3', ['v1', 'v3'], ['s3', '']));
    });

    it("hasClaims multiple values and scopes", function()
    {
        let identity = new Identity();
        identity.claims = [{type:'t1', value:'v1', scope: 's1'}, {type:'t1', value:'v2', scope: 's2'}, {type:'t3', value:'v3', scope:'s3'}, {type:'t4', value:'v4'}, {type:'t5', value:'v5', scope: ['s51','s52']}];

        assert.isFalse(identity.hasClaim('t1', ['v1', 'v2'], ['s3', '']));
        assert.isTrue(identity.hasClaim('t1', ['v1', 'v2'], ['s3', 's2']));
        assert.isTrue(identity.hasClaim('t1', ['v1', 'v2'], ['s3', 's1']));
        assert.isTrue(identity.hasClaim('t4', ['v1', 'v4'], ['s3', '']));

        assert.isTrue(identity.hasClaim('t5', ['v5', 'v4'], ['s3', 's51']));
        assert.isTrue(identity.hasClaim('t5', ['v5', 'v4'], ['s3', 's52']));
        assert.isFalse(identity.hasClaim('t4', ['v5', 'v4'], ['s3', 's52']));
        assert.isFalse(identity.hasClaim('t5', ['v2', 'v4'], ['s3', 's52']));
        assert.isFalse(identity.hasClaim('t5', ['v5', 'v4'], ['s3', '']));
    });

    it("getClaims", function()
    {
        let identity = new Identity();
        identity.claims = [{type:'t1', value:'v1'},{type:'t2', value:'v2'},{type:'t1', value:'v1'},{type:'t3',value:'v3'},{type:'t4',value:'v4',scope:['s41','s42']},{type:'t5',value:'v5',scope:'s5'}];

        assert.equal(identity.getClaims('t1', 'v1').length, 2);
        assert.equal(identity.getClaims('t2', 'v2').length, 1);
        assert.equal(identity.getClaims('t1', 'x').length, 0);
        assert.equal(identity.getClaims('x', 'v1').length, 0);
        assert.equal(identity.getClaims('x', 'x').length, 0);
        assert.equal(identity.getClaims('t4', 'v4').length, 0);
        assert.equal(identity.getClaims('t4', 'v4', 's41').length, 1);
        assert.equal(identity.getClaims('t4', 'v4', 's42').length, 1);
        assert.equal(identity.getClaims('t4', 'v4', 's4').length, 0);
        assert.equal(identity.getClaims('t5', 'v5').length, 0);
        assert.equal(identity.getClaims('t5', 'v5', '').length, 0);
        assert.equal(identity.getClaims('t5', 'v5', 's5').length, 1);
        assert.equal(identity.getClaims('t5', 'v5', 's41').length, 0);
    });

    it("getClaim", function()
    {
        let identity = new Identity();
        identity.claims = [{type:'t1', value:'v1'},{type:'t2', value:'v2'},{type:'t1', value:'v1'},{type:'t4',value:'v4',scope:'s4'},{type:'t5',value:'v5',scope:['s51','s52']}];

        assert.isDefined(identity.getClaim('t1', 'v1'));
        assert.isUndefined(identity.getClaim('t1', 'x'));
        assert.isUndefined(identity.getClaim('x', 'v1'));
        assert.isUndefined(identity.getClaim('x', 'x'));

        assert.isUndefined(identity.getClaim('t4', 'v4'));
        assert.isDefined(identity.getClaim('t4', 'v4', 's4'));
        
        assert.isUndefined(identity.getClaim('t5', 'v5'));
        assert.isDefined(identity.getClaim('t5', 'v5', 's51'));
        assert.isDefined(identity.getClaim('t5', 'v5', 's52'));
    });
});