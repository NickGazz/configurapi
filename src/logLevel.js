module.exports = {
    Debug : 'debug', 
    Trace : 'trace',
    Info : 'info',
    Error : 'error',
    None: 'none'
}
