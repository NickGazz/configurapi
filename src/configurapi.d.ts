///////////////////////////////
//
// Const
//
///////////////////////////////

export const Result: {
    Completed: number;
    Continue: number;
};

export const LogLevel : {
    Trace:string;
    Debug:string;
    Log:string;
    Error:string;
}

///////////////////////////////
//
// Interface
//
///////////////////////////////

export interface IConfig {}

export interface IClaim {
    type: string;
    value: string;
    scope: string|string[];
    
    //A helper property to return all scopes in an array.
    readonly scopes: string[];
}

export interface IIdentity {
    id: string;
    issuer: string;
    name: string;
    email: string;

    claims: IClaim[];

    hasClaim(type:string, value:string|string[], scope?:string|string[]): boolean;
    getClaim(type:string, value:string, scope?:string): IClaim;
    getClaims(type:string, value:string, scope?:string): IClaim[];
}

export interface IService {
    on(type:string, listener:ServiceListener): void;
    process(event:IEvent):Promise<void>;
}

export interface IEvent {
    id: string;
    correlationId: string;
    params: {};
    name: string;
    method: string;
    request: IRequest;
    response: IResponse;
    payload: any;
    identity: IIdentity;

    resolve(str:string): string;
}

export interface IRequest {
    name: string;
    method: string;
    headers: {};
    params: {};
    payload: string;
    query: string;
    path: string;
    pathAndQuery: string;
}

export interface IResponse {
    body: any;
    headers: {};
    statusCode: number;
}

export interface ServiceListener{
    (data:string):void;
}

///////////////////////////////
//
// Implementation
//
///////////////////////////////

export class Identity implements IIdentity {
    id: string;
    claims: IClaim[];
    issuer: string;
    name: string;
    email: string;

    constructor(id:string, claims?:IClaim[]);

    hasClaim(type:string, value:string|string[], scope?:string|string[]): boolean;
    getClaim(type:string, value:string, scope?:string): IClaim;
    getClaims(type:string, value:string, scope?:string): IClaim[];
}

export class Request implements IRequest {
    name: string;
    method: string;
    headers: {};
    params: {};
    payload: string;
    query: string;
    path: string;
    pathAndQuery: string;

    constructor(method:string);
}

export class Event implements IEvent {
    id: string;
    correlationId: string;
    params: {};
    name: string;
    method: string;
    request: IRequest;
    response: IResponse;
    payload: any;
    identity: IIdentity;

    constructor(request:IRequest);
    resolve(str:string): string;
}

export class Response implements IResponse {
    body: any;
    headers: {};
    statusCode: number; 

    constructor(body?:any, statusCode?:number, headers?:{});
}

export class ErrorResponse extends Response {
    message:any;
    details:string;
    constructor(message:any, statusCode?:number, details?:string);
}

export class Config implements IConfig {
    static load(path:string): IConfig;
    static parse(content:string): IConfig;
}

export class Service implements IService {
    constructor(config:Config);

    on(type:string, listener:ServiceListener): void;
    process(event:IEvent):Promise<void>;
}
