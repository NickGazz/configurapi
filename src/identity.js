let Dictionary = require('./dict');

module.exports = class Identity 
{
    get claims()
    {
        return this._claims;
    }

    set claims(value)
    {
        this._claims = value;
        
        this._claimsTable.clear();

        for(let claim of this._claims)
        {
            let scopes = Array.isArray(claim.scope) ? claim.scope : [claim.scope];

            for(let scope of scopes)
            {
                this._claimsTable.set(this.getClaimId(claim.type, claim.value, scope), claim);
            }
        }
    }

    getClaimId(type, value, scope)
    {
        return `${scope || ''}::${type || ''}::${value || ''}`;
    }

    constructor(id, claims = [])
    {
        this._claims = [];
        this._claimsTable = new Dictionary();

        this.id = id;
        this.claims = claims;
        this.issuer = undefined;
        this.name = undefined;
        this.email = undefined;
    }

    hasClaim(type, valueOrValues, scopeOrScopes)
    {
        if(!this.claims) return false;

        let values = Array.isArray(valueOrValues) ? [...new Set(valueOrValues)] : [valueOrValues];

        let scopes = Array.isArray(scopeOrScopes) ? [...new Set(scopeOrScopes)] : [scopeOrScopes];

        for(let value of values)
        {
            for(let scope of scopes)
            {
                if(this._claimsTable.has(this.getClaimId(type, value, scope)))
                {
                    return true;
                }
            }
        }

        return false;
    }

    getClaim(type, value, scope)
    {
        if(!this._claimsTable.has(this.getClaimId(type, value, scope))) return undefined;

        return this._claimsTable.get(this.getClaimId(type, value, scope));
    }

    getClaims(type, value, scope)
    {
        if(!this.claims) return [];

        let result = [];

        let matchingTypeValueClaims = this.claims.filter((c) => c.type === type && c.value === value);

        for(let claim of matchingTypeValueClaims)
        {
            if(claim.scope === scope) 
            {
                result.push(claim);
                continue;
            }

            //For arrays
            let scopes = Array.isArray(claim.scope) ? claim.scope : [claim.scope];

            if(scopes.includes(scope))
            {
                result.push(claim);
            }
        }

        return result;
    }
};