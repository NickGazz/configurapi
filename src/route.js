const events = require('events');
const Result = require('../src/result');
const LogLevel = require('./logLevel');

function toLogLevelNumber(logLevel)
{
    switch(logLevel)
    {
        case LogLevel.Debug: return 100;
        case LogLevel.Trace: return 75;
        case LogLevel.Warn: return 50;
        case LogLevel.Info: return 25;
        case LogLevel.Error: return 10;
        default: return 1
    }
}

module.exports = class Route extends events.EventEmitter
{
    constructor()
    {   
        super();
        this.name = '';
        this.enabled = true;
        this.policies = [];
    }

    async process(event)
    {
        for(let policy of this.policies)
        {
            let Stopwatch = require("agstopwatch");

            var watch = new Stopwatch();
            watch.start();

            try
            {
                let result =  await policy.process(event);
                
                if(result == Result.Completed)
                {
                    break;
                }
            }
            catch(error)
            {
                throw error;
            }
            finally
            {
                watch.stop();

                if(toLogLevelNumber(policy.logLevel) >= toLogLevelNumber(LogLevel.Trace)) 
                {
                    this.emit(LogLevel.Trace, `${event.id} - ${event.correlationId} - Executed ${policy.name} policy [${watch.elapsed}ms]`);
                }
            }
        }
    }  
};