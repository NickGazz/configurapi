const http = require('http');
const Event = require('./event');
const PolicyHandlerLoader = require('./policy/policyHandlerLoader');
const Config = require('./config');
const ErrorResponse = require('./errorResponse');
const events = require('events');
const Route = require('./route');
const Stopwatch = require("agstopwatch");
const fs = require('fs');
const NestedError = require('nested-error-stacks');
const LogLevel = require('./logLevel');
var oneliner = require('one-liner');
const { Debug } = require('./logLevel');

function toLogLevelNumber(logLevel)
{
    switch(logLevel)
    {
        case LogLevel.Debug: return 100;
        case LogLevel.Trace: return 75;
        case LogLevel.Warn: return 50;
        case LogLevel.Info: return 25;
        case LogLevel.Error: return 10;
        default: return 1
    }
}

module.exports = class Service extends events.EventEmitter
{
    constructor(config)
    {
        super();

        this.config = config;

        let loader = new PolicyHandlerLoader();

        for(let module of this.config.modules)
        {
            loader.load(module);
        }

        let customHandlerPath = 'handlers';

        if(fs.existsSync(customHandlerPath))
        {
            loader.loadCustomHandlers("handlers");
        }
        else
        {
            this.emit(LogLevel.Trace, `configurapi -  - Warning. Custom handlers directory does not exist.`);
        }


        this.config.events.map((route, key) =>
        {
            let routeLogLevel = toLogLevelNumber(route.logLevel)
            
            for(let policy of route.policies)
            {
                policy.handler = loader.get(policy.name);

                if(policy.logLevel)
                {
                    let policyLogLevel = toLogLevelNumber(policy.logLevel)

                    if(policyLogLevel >= toLogLevelNumber(LogLevel.Trace)) policy.on(LogLevel.Trace, (s)=>this.emit(LogLevel.Trace, oneliner(s)));
                    if(policyLogLevel >= toLogLevelNumber(LogLevel.Debug)) policy.on(LogLevel.Debug, (s)=>this.emit(LogLevel.Debug, oneliner(s)));
                    if(policyLogLevel >= toLogLevelNumber(LogLevel.Error)) policy.on(LogLevel.Error, (s)=>this.emit(LogLevel.Error, oneliner(s)));
                }
                else
                {
                    if(routeLogLevel >= toLogLevelNumber(LogLevel.Trace)) policy.on(LogLevel.Trace, (s)=>this.emit(LogLevel.Trace, oneliner(s)));
                    if(routeLogLevel >= toLogLevelNumber(LogLevel.Debug)) policy.on(LogLevel.Debug, (s)=>this.emit(LogLevel.Debug, oneliner(s)));
                    if(routeLogLevel >= toLogLevelNumber(LogLevel.Error)) policy.on(LogLevel.Error, (s)=>this.emit(LogLevel.Error, oneliner(s)));
                }
            }

            if(routeLogLevel >= toLogLevelNumber(LogLevel.Trace)) route.on(LogLevel.Trace, (s)=>this.emit(LogLevel.Trace, oneliner(s)));
            if(routeLogLevel >= toLogLevelNumber(LogLevel.Debug)) route.on(LogLevel.Debug, (s)=>this.emit(LogLevel.Debug, oneliner(s)));
            if(routeLogLevel >= toLogLevelNumber(LogLevel.Error)) route.on(LogLevel.Error, (s)=>this.emit(LogLevel.Error, oneliner(s)));

            this.emit(LogLevel.Trace, `configurapi -  - Registerd ${route.name} route.`);
        });

        this._handleEvent('on_start');
    }

    async process(event) 
    {
        let watch = new Stopwatch();

        try
        {
            watch.start();

            await this._handleEvent('on_before_request', event);
            
            if(await this._handleEvent(event.name, event) || await this._handleEvent(event.method, event))
            {
                //Great
            }
            else if(await this._handleEvent('catchall', event))
            {
                //Great
            }
            else
            {
                throw new ErrorResponse('Not Implemented.', 501, `Route for '${event.name}' event could not be found.'`);
            }

            if(event.response && event.response instanceof ErrorResponse)
            {
                throw event.response;
            }

            await this._handleEvent('on_success', event);
        }
        catch(error)
        {
            await this._handleError(event, error);
        }
        finally
        {
            try
            {
                await this._handleEvent('on_after_request', event);
            }
            catch(error)
            {
                 await this._handleError(event, error);
            }
            
            watch.stop();

            let route = this.config.events.get(event.name);
            
            if(route)
            {
                if(toLogLevelNumber(route.logLevel) >= toLogLevelNumber(LogLevel.Trace))
                {
                    this.emit(LogLevel.Trace, `${event.id} - ${event.correlationId} - Handled ${event.name} [${watch.elapsed}ms]`);
                }
            }
        }
    }

    async _handleEvent(eventName, event)
    {
        let result = false;

        if(this.config.events.has(eventName))
        {
            await this.config.events.get(eventName).process(event);

            result = true;
        }

        return result;
    }

    async _handleError(event, error)
    {
        if(error instanceof Error)
        {
            this.emit(LogLevel.Error,  `${event.id} - ${event.correlationId} - Failed to handle ${event.name}: ${error.message}`);

            event.response = new ErrorResponse(error, error['statusCode'] || 500);
        }
        else if(error instanceof ErrorResponse || ErrorResponse.isErrorResponse(error))
        {
            this.emit(LogLevel.Error, `${event.id} - ${event.correlationId} - Failed to handle ${event.name}: ${error.error}`);
            
            event.response = error;
        }
        else
        {
            this.emit(LogLevel.Error, `${event.id} - ${event.correlationId} - Failed to handle ${event.name}: ${error === undefined ? 'undefined' : error.toString()}`);

            event.response = new ErrorResponse("Internal Server Error", 500, error.toString());
        }

        if(this.config.events.has('on_error'))
        {
            try
            {
                this.emit(LogLevel.Error, `${event.id} - ${event.correlationId} - Failed to handle ${event.name}: ${error}`);

                await this._handleEvent('on_error', event);
            }
            catch(reason)
            {
                this.emit(LogLevel.Error, `${event.id} - ${event.correlationId} - ${reason}`);
            }
            return;
        }
    }
};