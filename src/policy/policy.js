const Result = require('../result');
const getParams = require('get-parameter-names');
const EventEmitter = require('events').EventEmitter;
const LogLevel = require('../logLevel')

module.exports = class Policy extends EventEmitter
{
    constructor()
    {
        super();
        
        this.name = "";
        this.handler = undefined;
        this.parameters = [];
        this.logLevel = LogLevel.Trace
    }

    async process(event)
    {
        return new Promise(async (resolve, reject) =>{
                
                let state = Result.complete;

                let context = {
                    'continue': () => state = Result.Continue,
                    'complete': () => state = Result.Completed,
                    'catch': (error) => reject(error),
                    'emit': (level,message) => this.emit(level, `${event.id} - ${event.correlationId} - ${message}`)
                };

                let handlerParams = this.parameters instanceof Array ? this.parameters.slice(0) : [];
                handlerParams.unshift(event);

                let something = this.handler.apply(context, handlerParams);
                
                if(something instanceof Promise)
                {
                    something.then(()=>
                    {
                        resolve(state);
                    })
                    .catch((error)=>
                    {
                        reject(error);
                    })
                }
                else
                {
                    resolve(state);
                }
            });
    }
};