let Dictionary = require('./dict');
let Route = require('./route');
let Policy = require('./policy/policy');
let LogLevel = require('./logLevel');
let yaml = require('js-yaml');
let fs = require('fs');
let NestedError = require('nested-error-stacks');

module.exports = class Config
{
    constructor()
    {
        this.modules = [];
        this.events = new Dictionary();
    }

    static load(path)
    {
        let content = "";

        try
        {
            content = fs.readFileSync(path, 'utf8');
        }
        catch(error)
        {
            throw new NestedError(`Failed to load '${path}'`, error);
        }

        return this.parse(content);
    }

    static parse(content)
    {
        
        let document = yaml.safeLoad(content);
        
        if(document instanceof Object)
        {
            let config = new Config();
            
            config.modules = this.parseImport(document);
            config.events = this.parseApi(document);

            return config;
        }
        else        
        {
            throw new Error("The specified config file is not an invalid yaml.");
        }
    }

    static parseImport(document) 
    {
        let result = [];

        //Check for an optional "import"
        if('import' in document)
        {
            if(typeof document['import'][Symbol.iterator] !== 'function')
            {
                throw new Error(`Could not load 'import.${document['import']}' in the config document`);
            }
            
            for(let module of document['import'])
            {
                result.push(module);
            }
        } 
        
        return result;
    }

    static parseApi(document)
    {
        if('api' in document)
        {
            return this.parseRoutes(document.api);
        }
        else
        {
            throw new Error('Could not find \'api\' in the config document');
        }
    }

    static parseRoutes(api)
    {
        if((api instanceof Object) && 'events' in api)
        {
            let result = new Dictionary();
            
            if(typeof api.events[Symbol.iterator] !== 'function')
            {
                throw new Error(`Events could not be empty in the config document.`);
            }

            let routeIndex=0;
            for(let r of api.events)
            {
                let route = this.parseRoute(r, routeIndex);

                if(route.enabled)
                {
                    result.set(route.name, route);
                }
            }

            return result;
        }
        else
        {
            throw new Error('Could not find \'api.events\' in the config document');
        }
    }

    static parseRoute(route, routeIndex)
    {
        let result = new Route();
       
        if(route instanceof Object && 'name' in route)
        {
            result.name = route.name;
            result.logLevel = LogLevel[Object.keys(LogLevel).find(key => key.toLowerCase() === (route.logLevel || LogLevel.Trace).toLowerCase())];

            if(route.when)
            {
                if(process.env.NODE_ENV === undefined)
                {
                    result.enabled = false;
                } 
                else
                {
                    result.enabled = process.env.NODE_ENV === route.when;
                }
            }

            result.policies = this.parsePolicies(route, result.name);
            
            return result;
        }
        else
        {
            throw new Error(`Could not find 'api.events[${routeIndex}].name' in the config document`);
        }
    }

    static parsePolicies(policies, routeName)
    {
        let result = [];
        
        if(policies instanceof Object && 'policies' in policies)
        {
            let policyIndex=0;

            if(typeof policies.policies[Symbol.iterator] !== 'function')
            {
                throw new Error(`Policies 'api.events[${routeName}].policies' could not be empty in the config document.`);
            }

            for(let policy of policies.policies)
            {
                result.push(this.parsePolicy(policy, routeName, policyIndex));
            }

            return result;
        }
        else
        {
            throw new Error(`Could not find 'api.events[${routeName}].policies' in the config document`);
        }
    }

    static parsePolicy(policy, routeName, policyIndex)
    {
        let result = new Policy();
        let policyName = this.parsePolicyName(policy);
        
        if(!policyName)
        {
            throw new Error(`Could not find 'api.events[${routeName}].policies[${policyIndex}]' in the config document`);
        }

        result.name = policyName;
        result.parameters = this.parseParameter(policy, routeName, policyName);
        result.logLevel = LogLevel[Object.keys(LogLevel).find(key => key.toLowerCase() === (policy.logLevel || LogLevel.Trace).toLowerCase())];
        return result;
    }

    static parsePolicyName(policy)
    {
        if(typeof policy == 'string')
        {
            return policy;
        }
        
        for (var key in policy) {
            if (policy.hasOwnProperty(key)) {
                return key;
            }
            return undefined;
        }
    }

    static parseParameter(policy, routeName, policyName)
    {
        if (policy[policyName] instanceof Array) 
        {
            return policy[policyName]
        }
        
        return policy.parameters || []
    }
};