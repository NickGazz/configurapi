module.exports = class Request
{
    constructor(method)
    {
        this.name = undefined;
        this.method = (method || "get").toLowerCase();
        this.headers = {};
        this.params = {};
        this.payload = "";
        this.query = "";
        this.path = "";
        this.pathAndQuery = "";
    }
};